﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AgregarOferta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AgregarOferta))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.dgOfertas = New System.Windows.Forms.DataGridView()
        Me.btnAgregarOferta = New System.Windows.Forms.Button()
        CType(Me.dgOfertas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(425, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(172, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "NOMBRE DE LA OFERTA"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(425, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "VALOR $"
        '
        'txtNombre
        '
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(629, 55)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(300, 30)
        Me.txtNombre.TabIndex = 2
        '
        'txtValor
        '
        Me.txtValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValor.Location = New System.Drawing.Point(629, 141)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(300, 30)
        Me.txtValor.TabIndex = 3
        '
        'dgOfertas
        '
        Me.dgOfertas.AllowUserToAddRows = False
        Me.dgOfertas.AllowUserToDeleteRows = False
        Me.dgOfertas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgOfertas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgOfertas.Location = New System.Drawing.Point(43, 242)
        Me.dgOfertas.Name = "dgOfertas"
        Me.dgOfertas.ReadOnly = True
        Me.dgOfertas.RowHeadersWidth = 51
        Me.dgOfertas.RowTemplate.Height = 24
        Me.dgOfertas.Size = New System.Drawing.Size(1311, 329)
        Me.dgOfertas.TabIndex = 4
        '
        'btnAgregarOferta
        '
        Me.btnAgregarOferta.Location = New System.Drawing.Point(704, 646)
        Me.btnAgregarOferta.Name = "btnAgregarOferta"
        Me.btnAgregarOferta.Size = New System.Drawing.Size(164, 78)
        Me.btnAgregarOferta.TabIndex = 5
        Me.btnAgregarOferta.Text = "AGREGAR"
        Me.btnAgregarOferta.UseVisualStyleBackColor = True
        '
        'AgregarOferta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1397, 786)
        Me.Controls.Add(Me.btnAgregarOferta)
        Me.Controls.Add(Me.dgOfertas)
        Me.Controls.Add(Me.txtValor)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AgregarOferta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AGREGAR OFERTA"
        CType(Me.dgOfertas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents txtValor As TextBox
    Friend WithEvents dgOfertas As DataGridView
    Friend WithEvents btnAgregarOferta As Button
End Class
