﻿Imports System.Data.SqlClient

Public Class AgregarOferta

    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim i As Integer

    Private Sub AgregarOferta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
        mostrar_ofertas()

    End Sub

    Public Sub mostrar_ofertas()

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select id as ID, name as 'NOMBRE DEL PRODUCTO', price as PRECIO, type as 'TIPO DE PRODUCTO' from Product where type = 'OFERTA' and status = 1"
        cmd.ExecuteNonQuery()
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        dgOfertas.DataSource = dt

    End Sub

    Private Sub btnAgregarOferta_Click(sender As Object, e As EventArgs) Handles btnAgregarOferta.Click
        Dim cont As Integer

        If txtNombre.Text.Trim.Equals("") Then
            cont += 1
        End If
        If txtValor.Text.Trim.Equals("") Then
            cont += 1
        End If

        If cont = 0 Then
            cmd = con.CreateCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "insert into Product values('" + txtNombre.Text + "','" + txtValor.Text + "' , 'oferta' , '1')"
            cmd.ExecuteNonQuery()
            MessageBox.Show("OFERTA AGREGADA")
            mostrar_ofertas()
            txtNombre.Text = ""
            txtValor.Text = ""

        Else
            MessageBox.Show("COMPLETAR TODOS LOS CAMPOS")

        End If








    End Sub

    Private Sub txtValor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtValor.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
        If Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            MsgBox("SOLO PUEDE DIGITAR NÚMEROS")
        End If
    End Sub
End Class