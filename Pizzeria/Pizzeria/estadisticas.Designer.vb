﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ESTADISTICAS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ESTADISTICAS))
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.txtFechaIni = New System.Windows.Forms.MaskedTextBox()
        Me.txtFechaFin = New System.Windows.Forms.MaskedTextBox()
        Me.cboTipoEntrega = New System.Windows.Forms.ComboBox()
        Me.cboPaymentType = New System.Windows.Forms.ComboBox()
        Me.dgEstadisticas = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        CType(Me.dgEstadisticas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(902, 557)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(207, 64)
        Me.btnBuscar.TabIndex = 2
        Me.btnBuscar.Text = "BUSCAR"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'txtFechaIni
        '
        Me.txtFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFechaIni.Location = New System.Drawing.Point(147, 62)
        Me.txtFechaIni.Mask = "##/##/####"
        Me.txtFechaIni.Name = "txtFechaIni"
        Me.txtFechaIni.PromptChar = Global.Microsoft.VisualBasic.ChrW(35)
        Me.txtFechaIni.Size = New System.Drawing.Size(128, 30)
        Me.txtFechaIni.TabIndex = 3
        '
        'txtFechaFin
        '
        Me.txtFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFechaFin.Location = New System.Drawing.Point(397, 62)
        Me.txtFechaFin.Mask = "##/##/####"
        Me.txtFechaFin.Name = "txtFechaFin"
        Me.txtFechaFin.PromptChar = Global.Microsoft.VisualBasic.ChrW(35)
        Me.txtFechaFin.Size = New System.Drawing.Size(121, 30)
        Me.txtFechaFin.TabIndex = 4
        '
        'cboTipoEntrega
        '
        Me.cboTipoEntrega.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoEntrega.FormattingEnabled = True
        Me.cboTipoEntrega.Items.AddRange(New Object() {"LOCAL", "DELIVERY"})
        Me.cboTipoEntrega.Location = New System.Drawing.Point(724, 62)
        Me.cboTipoEntrega.Name = "cboTipoEntrega"
        Me.cboTipoEntrega.Size = New System.Drawing.Size(163, 33)
        Me.cboTipoEntrega.TabIndex = 5
        '
        'cboPaymentType
        '
        Me.cboPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentType.FormattingEnabled = True
        Me.cboPaymentType.Items.AddRange(New Object() {"CREDITO", "DEBITO", "EFECTIVO"})
        Me.cboPaymentType.Location = New System.Drawing.Point(1138, 62)
        Me.cboPaymentType.Name = "cboPaymentType"
        Me.cboPaymentType.Size = New System.Drawing.Size(158, 33)
        Me.cboPaymentType.TabIndex = 6
        '
        'dgEstadisticas
        '
        Me.dgEstadisticas.AllowUserToAddRows = False
        Me.dgEstadisticas.AllowUserToDeleteRows = False
        Me.dgEstadisticas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgEstadisticas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstadisticas.Location = New System.Drawing.Point(32, 143)
        Me.dgEstadisticas.Name = "dgEstadisticas"
        Me.dgEstadisticas.ReadOnly = True
        Me.dgEstadisticas.RowHeadersWidth = 51
        Me.dgEstadisticas.RowTemplate.Height = 24
        Me.dgEstadisticas.Size = New System.Drawing.Size(1257, 266)
        Me.dgEstadisticas.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "FECHA INICIAL"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(296, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 17)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "FECHA FINAL"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(563, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 17)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "TIPO DE ENTREGA"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(976, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(124, 17)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "FORMA DE PAGO"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(955, 452)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "TOTAL"
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(1138, 449)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(151, 30)
        Me.txtTotal.TabIndex = 13
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Location = New System.Drawing.Point(164, 557)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(207, 64)
        Me.btnLimpiar.TabIndex = 14
        Me.btnLimpiar.Text = "LIMPIAR FILTROS"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'ESTADISTICAS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1360, 682)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgEstadisticas)
        Me.Controls.Add(Me.cboPaymentType)
        Me.Controls.Add(Me.cboTipoEntrega)
        Me.Controls.Add(Me.txtFechaFin)
        Me.Controls.Add(Me.txtFechaIni)
        Me.Controls.Add(Me.btnBuscar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "ESTADISTICAS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ESTADISTICAS"
        CType(Me.dgEstadisticas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBuscar As Button
    Friend WithEvents txtFechaIni As MaskedTextBox
    Friend WithEvents txtFechaFin As MaskedTextBox
    Friend WithEvents cboTipoEntrega As ComboBox
    Friend WithEvents cboPaymentType As ComboBox
    Friend WithEvents dgEstadisticas As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtTotal As TextBox
    Friend WithEvents btnLimpiar As Button
End Class
