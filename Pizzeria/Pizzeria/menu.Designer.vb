﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class menu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(menu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.VENDERToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REALIZARVENTAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MODIFICARVENTAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MANTENEDORToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PRODUCTOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AGREGARToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MODIFCARToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OFERTAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AGREGARToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MODIFICARToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ENVASESToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AGREGARToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ESTADISTICASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblHora = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.horaFecha = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 15.0!)
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VENDERToolStripMenuItem, Me.MANTENEDORToolStripMenuItem, Me.ESTADISTICASToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1048, 36)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'VENDERToolStripMenuItem
        '
        Me.VENDERToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.REALIZARVENTAToolStripMenuItem, Me.MODIFICARVENTAToolStripMenuItem})
        Me.VENDERToolStripMenuItem.Name = "VENDERToolStripMenuItem"
        Me.VENDERToolStripMenuItem.Size = New System.Drawing.Size(94, 32)
        Me.VENDERToolStripMenuItem.Text = "VENTAS"
        '
        'REALIZARVENTAToolStripMenuItem
        '
        Me.REALIZARVENTAToolStripMenuItem.Name = "REALIZARVENTAToolStripMenuItem"
        Me.REALIZARVENTAToolStripMenuItem.Size = New System.Drawing.Size(252, 32)
        Me.REALIZARVENTAToolStripMenuItem.Text = "REALIZAR VENTA"
        '
        'MODIFICARVENTAToolStripMenuItem
        '
        Me.MODIFICARVENTAToolStripMenuItem.Name = "MODIFICARVENTAToolStripMenuItem"
        Me.MODIFICARVENTAToolStripMenuItem.Size = New System.Drawing.Size(252, 32)
        Me.MODIFICARVENTAToolStripMenuItem.Text = "MODIFICAR VENTA"
        '
        'MANTENEDORToolStripMenuItem
        '
        Me.MANTENEDORToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PRODUCTOToolStripMenuItem, Me.OFERTAToolStripMenuItem, Me.ENVASESToolStripMenuItem})
        Me.MANTENEDORToolStripMenuItem.Name = "MANTENEDORToolStripMenuItem"
        Me.MANTENEDORToolStripMenuItem.Size = New System.Drawing.Size(156, 32)
        Me.MANTENEDORToolStripMenuItem.Text = "MANTENEDOR"
        '
        'PRODUCTOToolStripMenuItem
        '
        Me.PRODUCTOToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AGREGARToolStripMenuItem, Me.MODIFCARToolStripMenuItem})
        Me.PRODUCTOToolStripMenuItem.Name = "PRODUCTOToolStripMenuItem"
        Me.PRODUCTOToolStripMenuItem.Size = New System.Drawing.Size(186, 32)
        Me.PRODUCTOToolStripMenuItem.Text = "PRODUCTO"
        '
        'AGREGARToolStripMenuItem
        '
        Me.AGREGARToolStripMenuItem.Name = "AGREGARToolStripMenuItem"
        Me.AGREGARToolStripMenuItem.Size = New System.Drawing.Size(183, 32)
        Me.AGREGARToolStripMenuItem.Text = "AGREGAR"
        '
        'MODIFCARToolStripMenuItem
        '
        Me.MODIFCARToolStripMenuItem.Name = "MODIFCARToolStripMenuItem"
        Me.MODIFCARToolStripMenuItem.Size = New System.Drawing.Size(183, 32)
        Me.MODIFCARToolStripMenuItem.Text = "MODIFCAR"
        '
        'OFERTAToolStripMenuItem
        '
        Me.OFERTAToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AGREGARToolStripMenuItem1, Me.MODIFICARToolStripMenuItem})
        Me.OFERTAToolStripMenuItem.Name = "OFERTAToolStripMenuItem"
        Me.OFERTAToolStripMenuItem.Size = New System.Drawing.Size(186, 32)
        Me.OFERTAToolStripMenuItem.Text = "OFERTA"
        '
        'AGREGARToolStripMenuItem1
        '
        Me.AGREGARToolStripMenuItem1.Name = "AGREGARToolStripMenuItem1"
        Me.AGREGARToolStripMenuItem1.Size = New System.Drawing.Size(188, 32)
        Me.AGREGARToolStripMenuItem1.Text = "AGREGAR"
        '
        'MODIFICARToolStripMenuItem
        '
        Me.MODIFICARToolStripMenuItem.Name = "MODIFICARToolStripMenuItem"
        Me.MODIFICARToolStripMenuItem.Size = New System.Drawing.Size(188, 32)
        Me.MODIFICARToolStripMenuItem.Text = "MODIFICAR"
        '
        'ENVASESToolStripMenuItem
        '
        Me.ENVASESToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AGREGARToolStripMenuItem2})
        Me.ENVASESToolStripMenuItem.Name = "ENVASESToolStripMenuItem"
        Me.ENVASESToolStripMenuItem.Size = New System.Drawing.Size(186, 32)
        Me.ENVASESToolStripMenuItem.Text = "ENVASES"
        '
        'AGREGARToolStripMenuItem2
        '
        Me.AGREGARToolStripMenuItem2.Name = "AGREGARToolStripMenuItem2"
        Me.AGREGARToolStripMenuItem2.Size = New System.Drawing.Size(188, 32)
        Me.AGREGARToolStripMenuItem2.Text = "MODIFICAR"
        '
        'ESTADISTICASToolStripMenuItem
        '
        Me.ESTADISTICASToolStripMenuItem.Name = "ESTADISTICASToolStripMenuItem"
        Me.ESTADISTICASToolStripMenuItem.Size = New System.Drawing.Size(148, 32)
        Me.ESTADISTICASToolStripMenuItem.Text = "ESTADISTICAS"
        '
        'lblHora
        '
        Me.lblHora.AutoSize = True
        Me.lblHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHora.Location = New System.Drawing.Point(472, 464)
        Me.lblHora.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(126, 39)
        Me.lblHora.TabIndex = 1
        Me.lblHora.Text = "Label1"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(350, 520)
        Me.lblFecha.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(126, 39)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Label2"
        '
        'horaFecha
        '
        Me.horaFecha.Enabled = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Pizzeria.My.Resources.Resources.amuyenR
        Me.PictureBox1.Location = New System.Drawing.Point(324, 82)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(406, 336)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1048, 639)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblHora)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MaximizeBox = False
        Me.Name = "menu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MENU"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents VENDERToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents REALIZARVENTAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MODIFICARVENTAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MANTENEDORToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PRODUCTOToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AGREGARToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MODIFCARToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OFERTAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AGREGARToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents MODIFICARToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ESTADISTICASToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblHora As Label
    Friend WithEvents lblFecha As Label
    Friend WithEvents horaFecha As Timer
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ENVASESToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AGREGARToolStripMenuItem2 As ToolStripMenuItem
End Class
