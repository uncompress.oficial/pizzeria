﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modificarOferta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(modificarOferta))
        Me.dgEliminarOfertas = New System.Windows.Forms.DataGridView()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificarOferta = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        CType(Me.dgEliminarOfertas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgEliminarOfertas
        '
        Me.dgEliminarOfertas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgEliminarOfertas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEliminarOfertas.Location = New System.Drawing.Point(135, 62)
        Me.dgEliminarOfertas.Name = "dgEliminarOfertas"
        Me.dgEliminarOfertas.RowHeadersWidth = 51
        Me.dgEliminarOfertas.RowTemplate.Height = 24
        Me.dgEliminarOfertas.Size = New System.Drawing.Size(1198, 356)
        Me.dgEliminarOfertas.TabIndex = 0
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(1099, 490)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(234, 89)
        Me.btnEliminar.TabIndex = 1
        Me.btnEliminar.Text = "ELIMINAR"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificarOferta
        '
        Me.btnModificarOferta.Location = New System.Drawing.Point(135, 490)
        Me.btnModificarOferta.Name = "btnModificarOferta"
        Me.btnModificarOferta.Size = New System.Drawing.Size(234, 89)
        Me.btnModificarOferta.TabIndex = 2
        Me.btnModificarOferta.Text = "MODIFICAR"
        Me.btnModificarOferta.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(613, 490)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(220, 89)
        Me.btnUpdate.TabIndex = 3
        Me.btnUpdate.Text = "ACTUALIZAR"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'modificarOferta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1397, 786)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnModificarOferta)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.dgEliminarOfertas)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "modificarOferta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MODIFICAR OFERTA"
        CType(Me.dgEliminarOfertas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgEliminarOfertas As DataGridView
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnModificarOferta As Button
    Friend WithEvents btnUpdate As Button
End Class
