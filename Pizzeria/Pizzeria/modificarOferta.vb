﻿Imports System.Data.SqlClient

Public Class modificarOferta

    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim i As Integer


    Public Sub mostrar_ofertas()

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select id as ID, name as 'NOMBRE DEL PRODUCTO', price as PRECIO, type as 'TIPO DE PRODUCTO' from Product where type = 'OFERTA' and status = 1"
        cmd.ExecuteNonQuery()
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        dgEliminarOfertas.DataSource = dt

    End Sub

    Private Sub modificarOferta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
        mostrar_ofertas()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        If MessageBox.Show("SEGURO QUE DESEA ELIMINAR?", "ELIMINAR PRODUCTO", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            cmd.CommandText = "UPDATE Product SET status = 0  WHERE id = '" & dgEliminarOfertas.CurrentRow.Cells(0).Value & "'"
            cmd.ExecuteNonQuery()
            MessageBox.Show("OFERTA ELIMINADA")
            mostrar_ofertas()
        End If






    End Sub

    Private Sub btnModificarOferta_Click(sender As Object, e As EventArgs) Handles btnModificarOferta.Click

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "UPDATE Product SET name = '" & dgEliminarOfertas.CurrentRow.Cells(1).Value.ToString.ToUpper & "' , price = '" & dgEliminarOfertas.CurrentRow.Cells(2).Value & "'  WHERE id = '" & dgEliminarOfertas.CurrentRow.Cells(0).Value & "'"
        cmd.ExecuteNonQuery()
        MessageBox.Show("PRODUCTO MODIFICADO")
        mostrar_ofertas()
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

    End Sub
End Class