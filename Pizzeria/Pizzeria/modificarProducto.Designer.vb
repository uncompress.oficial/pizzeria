﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modificarProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(modificarProducto))
        Me.dgModificarProducto = New System.Windows.Forms.DataGridView()
        Me.btnModifiarProdcuto = New System.Windows.Forms.Button()
        Me.btnEliminarProducto = New System.Windows.Forms.Button()
        CType(Me.dgModificarProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgModificarProducto
        '
        Me.dgModificarProducto.AllowUserToAddRows = False
        Me.dgModificarProducto.AllowUserToDeleteRows = False
        Me.dgModificarProducto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgModificarProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgModificarProducto.Location = New System.Drawing.Point(75, 49)
        Me.dgModificarProducto.Name = "dgModificarProducto"
        Me.dgModificarProducto.RowHeadersWidth = 51
        Me.dgModificarProducto.RowTemplate.Height = 24
        Me.dgModificarProducto.Size = New System.Drawing.Size(1255, 376)
        Me.dgModificarProducto.TabIndex = 0
        '
        'btnModifiarProdcuto
        '
        Me.btnModifiarProdcuto.Location = New System.Drawing.Point(75, 477)
        Me.btnModifiarProdcuto.Name = "btnModifiarProdcuto"
        Me.btnModifiarProdcuto.Size = New System.Drawing.Size(279, 77)
        Me.btnModifiarProdcuto.TabIndex = 1
        Me.btnModifiarProdcuto.Text = "MODIFICAR"
        Me.btnModifiarProdcuto.UseVisualStyleBackColor = True
        '
        'btnEliminarProducto
        '
        Me.btnEliminarProducto.Location = New System.Drawing.Point(1051, 477)
        Me.btnEliminarProducto.Name = "btnEliminarProducto"
        Me.btnEliminarProducto.Size = New System.Drawing.Size(279, 77)
        Me.btnEliminarProducto.TabIndex = 2
        Me.btnEliminarProducto.Text = "ELIMINAR"
        Me.btnEliminarProducto.UseVisualStyleBackColor = True
        '
        'modificarProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1397, 786)
        Me.Controls.Add(Me.btnEliminarProducto)
        Me.Controls.Add(Me.btnModifiarProdcuto)
        Me.Controls.Add(Me.dgModificarProducto)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "modificarProducto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MODIFICAR PRODUCTO"
        CType(Me.dgModificarProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgModificarProducto As DataGridView
    Friend WithEvents btnModifiarProdcuto As Button
    Friend WithEvents btnEliminarProducto As Button
End Class
