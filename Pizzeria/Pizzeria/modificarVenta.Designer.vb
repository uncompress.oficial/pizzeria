﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modificarVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(modificarVenta))
        Me.dgModificarVenta = New System.Windows.Forms.DataGridView()
        Me.btnEliminarVenta = New System.Windows.Forms.Button()
        Me.btnImprimirVenta = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        CType(Me.dgModificarVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgModificarVenta
        '
        Me.dgModificarVenta.AllowUserToAddRows = False
        Me.dgModificarVenta.AllowUserToDeleteRows = False
        Me.dgModificarVenta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgModificarVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgModificarVenta.Location = New System.Drawing.Point(58, 46)
        Me.dgModificarVenta.Name = "dgModificarVenta"
        Me.dgModificarVenta.RowHeadersWidth = 51
        Me.dgModificarVenta.RowTemplate.Height = 24
        Me.dgModificarVenta.Size = New System.Drawing.Size(1219, 381)
        Me.dgModificarVenta.TabIndex = 0
        '
        'btnEliminarVenta
        '
        Me.btnEliminarVenta.Location = New System.Drawing.Point(58, 515)
        Me.btnEliminarVenta.Name = "btnEliminarVenta"
        Me.btnEliminarVenta.Size = New System.Drawing.Size(240, 104)
        Me.btnEliminarVenta.TabIndex = 1
        Me.btnEliminarVenta.Text = "ELIMINAR"
        Me.btnEliminarVenta.UseVisualStyleBackColor = True
        '
        'btnImprimirVenta
        '
        Me.btnImprimirVenta.Location = New System.Drawing.Point(1037, 515)
        Me.btnImprimirVenta.Name = "btnImprimirVenta"
        Me.btnImprimirVenta.Size = New System.Drawing.Size(240, 104)
        Me.btnImprimirVenta.TabIndex = 2
        Me.btnImprimirVenta.Text = "IMPRIMIR"
        Me.btnImprimirVenta.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(540, 515)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(235, 104)
        Me.btnUpdate.TabIndex = 3
        Me.btnUpdate.Text = "ACTUALIZAR"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'modificarVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1397, 786)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnImprimirVenta)
        Me.Controls.Add(Me.btnEliminarVenta)
        Me.Controls.Add(Me.dgModificarVenta)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "modificarVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MANTENEDOR VENTA"
        CType(Me.dgModificarVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgModificarVenta As DataGridView
    Friend WithEvents btnEliminarVenta As Button
    Friend WithEvents btnImprimirVenta As Button
    Friend WithEvents btnUpdate As Button
End Class
