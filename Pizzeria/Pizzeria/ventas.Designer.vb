﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ventas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ventas))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAddProduct = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboProducts = New System.Windows.Forms.ComboBox()
        Me.txtQuantity = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.dgvCart = New System.Windows.Forms.DataGridView()
        Me.product = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ingredients = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.price = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblClientName = New System.Windows.Forms.Label()
        Me.txtClientName = New System.Windows.Forms.TextBox()
        Me.btnStoreSale = New System.Windows.Forms.Button()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.txtChange = New System.Windows.Forms.TextBox()
        Me.txtPaymentValue = New System.Windows.Forms.TextBox()
        Me.lblChange = New System.Windows.Forms.Label()
        Me.lblPaymentType = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboDeliveryType = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnRemoveAll = New System.Windows.Forms.Button()
        Me.cboPaymentType = New System.Windows.Forms.ComboBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboSpecialIngredient = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAddCart = New System.Windows.Forms.Button()
        Me.cboExtraIngredient = New System.Windows.Forms.ComboBox()
        Me.cboSauce = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvCart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.Controls.Add(Me.btnAddProduct)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtDescription)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.cboProducts)
        Me.Panel1.Controls.Add(Me.txtQuantity)
        Me.Panel1.Location = New System.Drawing.Point(9, 9)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(515, 346)
        Me.Panel1.TabIndex = 0
        '
        'btnAddProduct
        '
        Me.btnAddProduct.BackColor = System.Drawing.SystemColors.Control
        Me.btnAddProduct.Location = New System.Drawing.Point(332, 271)
        Me.btnAddProduct.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAddProduct.Name = "btnAddProduct"
        Me.btnAddProduct.Size = New System.Drawing.Size(164, 41)
        Me.btnAddProduct.TabIndex = 10
        Me.btnAddProduct.Text = "AGREGAR PRODUCTO"
        Me.btnAddProduct.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(18, 74)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(168, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "DESCRIPCION DEL PRODUCTO"
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescription.Location = New System.Drawing.Point(20, 97)
        Me.txtDescription.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(478, 150)
        Me.txtDescription.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(390, 14)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "INGRESE CANTIDAD"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 14)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "SELECCIONE PRODUCTO"
        '
        'cboProducts
        '
        Me.cboProducts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProducts.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProducts.FormattingEnabled = True
        Me.cboProducts.Location = New System.Drawing.Point(20, 31)
        Me.cboProducts.Margin = New System.Windows.Forms.Padding(2)
        Me.cboProducts.Name = "cboProducts"
        Me.cboProducts.Size = New System.Drawing.Size(339, 28)
        Me.cboProducts.TabIndex = 1
        '
        'txtQuantity
        '
        Me.txtQuantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuantity.Location = New System.Drawing.Point(388, 33)
        Me.txtQuantity.Margin = New System.Windows.Forms.Padding(2)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(110, 26)
        Me.txtQuantity.TabIndex = 0
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.SystemColors.Control
        Me.btnClear.Location = New System.Drawing.Point(14, 214)
        Me.btnClear.Margin = New System.Windows.Forms.Padding(2)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(115, 67)
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "LIMPIAR"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'dgvCart
        '
        Me.dgvCart.AllowUserToAddRows = False
        Me.dgvCart.AllowUserToDeleteRows = False
        Me.dgvCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCart.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.product, Me.Cantidad, Me.ingredients, Me.price, Me.description})
        Me.dgvCart.Location = New System.Drawing.Point(2, 17)
        Me.dgvCart.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvCart.Name = "dgvCart"
        Me.dgvCart.ReadOnly = True
        Me.dgvCart.RowHeadersWidth = 51
        Me.dgvCart.RowTemplate.Height = 24
        Me.dgvCart.Size = New System.Drawing.Size(670, 206)
        Me.dgvCart.TabIndex = 3
        '
        'product
        '
        Me.product.HeaderText = "PRODUCTO"
        Me.product.MinimumWidth = 6
        Me.product.Name = "product"
        Me.product.ReadOnly = True
        Me.product.Width = 125
        '
        'Cantidad
        '
        Me.Cantidad.HeaderText = "CANTIDAD"
        Me.Cantidad.MinimumWidth = 6
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        Me.Cantidad.Width = 125
        '
        'ingredients
        '
        Me.ingredients.HeaderText = "INGREDIENTES"
        Me.ingredients.MinimumWidth = 6
        Me.ingredients.Name = "ingredients"
        Me.ingredients.ReadOnly = True
        Me.ingredients.ToolTipText = "AGREGADOS"
        Me.ingredients.Width = 125
        '
        'price
        '
        Me.price.HeaderText = "PRECIO"
        Me.price.MinimumWidth = 6
        Me.price.Name = "price"
        Me.price.ReadOnly = True
        Me.price.Width = 125
        '
        'description
        '
        Me.description.HeaderText = "DESCRIPCION"
        Me.description.MinimumWidth = 6
        Me.description.Name = "description"
        Me.description.ReadOnly = True
        Me.description.Width = 125
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel2.Controls.Add(Me.lblClientName)
        Me.Panel2.Controls.Add(Me.txtClientName)
        Me.Panel2.Controls.Add(Me.btnStoreSale)
        Me.Panel2.Controls.Add(Me.txtAddress)
        Me.Panel2.Controls.Add(Me.lblAddress)
        Me.Panel2.Controls.Add(Me.txtChange)
        Me.Panel2.Controls.Add(Me.txtPaymentValue)
        Me.Panel2.Controls.Add(Me.lblChange)
        Me.Panel2.Controls.Add(Me.lblPaymentType)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.cboDeliveryType)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.btnRemoveAll)
        Me.Panel2.Controls.Add(Me.dgvCart)
        Me.Panel2.Controls.Add(Me.cboPaymentType)
        Me.Panel2.Controls.Add(Me.txtTotal)
        Me.Panel2.Location = New System.Drawing.Point(529, 9)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(674, 670)
        Me.Panel2.TabIndex = 1
        '
        'lblClientName
        '
        Me.lblClientName.AutoSize = True
        Me.lblClientName.Location = New System.Drawing.Point(8, 306)
        Me.lblClientName.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.Size = New System.Drawing.Size(102, 13)
        Me.lblClientName.TabIndex = 11
        Me.lblClientName.Text = "NOMBRE CLIENTE"
        '
        'txtClientName
        '
        Me.txtClientName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClientName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClientName.Location = New System.Drawing.Point(139, 295)
        Me.txtClientName.Margin = New System.Windows.Forms.Padding(2)
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Size = New System.Drawing.Size(461, 26)
        Me.txtClientName.TabIndex = 11
        '
        'btnStoreSale
        '
        Me.btnStoreSale.BackColor = System.Drawing.SystemColors.Control
        Me.btnStoreSale.Location = New System.Drawing.Point(489, 451)
        Me.btnStoreSale.Margin = New System.Windows.Forms.Padding(2)
        Me.btnStoreSale.Name = "btnStoreSale"
        Me.btnStoreSale.Size = New System.Drawing.Size(110, 67)
        Me.btnStoreSale.TabIndex = 17
        Me.btnStoreSale.Text = "GENERAR VENTA"
        Me.btnStoreSale.UseVisualStyleBackColor = False
        '
        'txtAddress
        '
        Me.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(139, 351)
        Me.txtAddress.Margin = New System.Windows.Forms.Padding(2)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(461, 45)
        Me.txtAddress.TabIndex = 16
        Me.txtAddress.Visible = False
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(8, 364)
        Me.lblAddress.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(66, 13)
        Me.lblAddress.TabIndex = 15
        Me.lblAddress.Text = "DIRECCION"
        Me.lblAddress.Visible = False
        '
        'txtChange
        '
        Me.txtChange.Enabled = False
        Me.txtChange.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChange.Location = New System.Drawing.Point(139, 608)
        Me.txtChange.Margin = New System.Windows.Forms.Padding(2)
        Me.txtChange.Name = "txtChange"
        Me.txtChange.ReadOnly = True
        Me.txtChange.Size = New System.Drawing.Size(190, 26)
        Me.txtChange.TabIndex = 14
        Me.txtChange.Visible = False
        '
        'txtPaymentValue
        '
        Me.txtPaymentValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaymentValue.Location = New System.Drawing.Point(139, 530)
        Me.txtPaymentValue.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPaymentValue.Name = "txtPaymentValue"
        Me.txtPaymentValue.Size = New System.Drawing.Size(190, 26)
        Me.txtPaymentValue.TabIndex = 13
        Me.txtPaymentValue.Visible = False
        '
        'lblChange
        '
        Me.lblChange.AutoSize = True
        Me.lblChange.Location = New System.Drawing.Point(10, 608)
        Me.lblChange.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblChange.Name = "lblChange"
        Me.lblChange.Size = New System.Drawing.Size(56, 13)
        Me.lblChange.TabIndex = 12
        Me.lblChange.Text = "VUELTO$"
        Me.lblChange.Visible = False
        '
        'lblPaymentType
        '
        Me.lblPaymentType.AutoSize = True
        Me.lblPaymentType.Location = New System.Drawing.Point(10, 534)
        Me.lblPaymentType.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPaymentType.Name = "lblPaymentType"
        Me.lblPaymentType.Size = New System.Drawing.Size(43, 13)
        Me.lblPaymentType.TabIndex = 11
        Me.lblPaymentType.Text = "PAGO$"
        Me.lblPaymentType.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 245)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(118, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "FORMA DE ENTREGA"
        '
        'cboDeliveryType
        '
        Me.cboDeliveryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeliveryType.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeliveryType.FormattingEnabled = True
        Me.cboDeliveryType.Items.AddRange(New Object() {"LOCAL", "DELIVERY"})
        Me.cboDeliveryType.Location = New System.Drawing.Point(139, 242)
        Me.cboDeliveryType.Margin = New System.Windows.Forms.Padding(2)
        Me.cboDeliveryType.Name = "cboDeliveryType"
        Me.cboDeliveryType.Size = New System.Drawing.Size(158, 28)
        Me.cboDeliveryType.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(326, 248)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(96, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "FORMA DE PAGO"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 455)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "TOTAL $"
        '
        'btnRemoveAll
        '
        Me.btnRemoveAll.BackColor = System.Drawing.SystemColors.Control
        Me.btnRemoveAll.Location = New System.Drawing.Point(489, 574)
        Me.btnRemoveAll.Margin = New System.Windows.Forms.Padding(2)
        Me.btnRemoveAll.Name = "btnRemoveAll"
        Me.btnRemoveAll.Size = New System.Drawing.Size(110, 67)
        Me.btnRemoveAll.TabIndex = 3
        Me.btnRemoveAll.Text = "ELIMINAR VENTA"
        Me.btnRemoveAll.UseVisualStyleBackColor = False
        '
        'cboPaymentType
        '
        Me.cboPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentType.FormattingEnabled = True
        Me.cboPaymentType.Items.AddRange(New Object() {"CREDITO", "DEBITO", "EFECTIVO"})
        Me.cboPaymentType.Location = New System.Drawing.Point(447, 242)
        Me.cboPaymentType.Margin = New System.Windows.Forms.Padding(2)
        Me.cboPaymentType.Name = "cboPaymentType"
        Me.cboPaymentType.Size = New System.Drawing.Size(152, 28)
        Me.cboPaymentType.TabIndex = 1
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(139, 451)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(190, 26)
        Me.txtTotal.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.cboSpecialIngredient)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.btnClear)
        Me.Panel3.Controls.Add(Me.btnAddCart)
        Me.Panel3.Controls.Add(Me.cboExtraIngredient)
        Me.Panel3.Controls.Add(Me.cboSauce)
        Me.Panel3.Location = New System.Drawing.Point(9, 362)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(515, 316)
        Me.Panel3.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 135)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(135, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "INGREDIENTE ESPECIAL"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 89)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "INGREDIENTE EXTRA"
        '
        'cboSpecialIngredient
        '
        Me.cboSpecialIngredient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSpecialIngredient.Enabled = False
        Me.cboSpecialIngredient.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSpecialIngredient.FormattingEnabled = True
        Me.cboSpecialIngredient.Location = New System.Drawing.Point(242, 122)
        Me.cboSpecialIngredient.Margin = New System.Windows.Forms.Padding(2)
        Me.cboSpecialIngredient.Name = "cboSpecialIngredient"
        Me.cboSpecialIngredient.Size = New System.Drawing.Size(255, 28)
        Me.cboSpecialIngredient.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 38)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "SALSA"
        '
        'btnAddCart
        '
        Me.btnAddCart.BackColor = System.Drawing.SystemColors.Control
        Me.btnAddCart.Location = New System.Drawing.Point(388, 214)
        Me.btnAddCart.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAddCart.Name = "btnAddCart"
        Me.btnAddCart.Size = New System.Drawing.Size(110, 67)
        Me.btnAddCart.TabIndex = 2
        Me.btnAddCart.Text = "AGREGAR"
        Me.btnAddCart.UseVisualStyleBackColor = False
        '
        'cboExtraIngredient
        '
        Me.cboExtraIngredient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExtraIngredient.Enabled = False
        Me.cboExtraIngredient.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExtraIngredient.FormattingEnabled = True
        Me.cboExtraIngredient.Location = New System.Drawing.Point(242, 76)
        Me.cboExtraIngredient.Margin = New System.Windows.Forms.Padding(2)
        Me.cboExtraIngredient.Name = "cboExtraIngredient"
        Me.cboExtraIngredient.Size = New System.Drawing.Size(255, 28)
        Me.cboExtraIngredient.TabIndex = 1
        '
        'cboSauce
        '
        Me.cboSauce.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSauce.Enabled = False
        Me.cboSauce.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSauce.FormattingEnabled = True
        Me.cboSauce.Location = New System.Drawing.Point(242, 25)
        Me.cboSauce.Margin = New System.Windows.Forms.Padding(2)
        Me.cboSauce.Name = "cboSauce"
        Me.cboSauce.Size = New System.Drawing.Size(255, 28)
        Me.cboSauce.TabIndex = 0
        '
        'ventas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1224, 705)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.Name = "ventas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VENTAS"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvCart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnClear As Button
    Friend WithEvents dgvCart As DataGridView
    Friend WithEvents cboProducts As ComboBox
    Friend WithEvents txtQuantity As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnRemoveAll As Button
    Friend WithEvents cboPaymentType As ComboBox
    Friend WithEvents txtTotal As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cboSpecialIngredient As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents btnAddCart As Button
    Friend WithEvents cboExtraIngredient As ComboBox
    Friend WithEvents cboSauce As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtDescription As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cboDeliveryType As ComboBox
    Friend WithEvents txtChange As TextBox
    Friend WithEvents txtPaymentValue As TextBox
    Friend WithEvents lblChange As Label
    Friend WithEvents lblPaymentType As Label
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents lblAddress As Label
    Friend WithEvents btnStoreSale As Button
    Friend WithEvents btnAddProduct As Button
    Friend WithEvents lblClientName As Label
    Friend WithEvents txtClientName As TextBox
    Friend WithEvents product As DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As DataGridViewTextBoxColumn
    Friend WithEvents ingredients As DataGridViewTextBoxColumn
    Friend WithEvents price As DataGridViewTextBoxColumn
    Friend WithEvents description As DataGridViewTextBoxColumn
End Class
