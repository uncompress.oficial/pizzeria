﻿Imports System.Data.SqlClient
Imports Spire.Doc
Imports Spire.Doc.Documents
Imports Spire.Doc.Fields
Imports Spire.Pdf
Imports HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment
Public Class ventas


    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim cart As ArrayList
    Dim total As Integer
    Dim delivered As Boolean = False

    Private Sub btnAddCart_Click(sender As Object, e As EventArgs) Handles btnAddCart.Click

        Dim cont As Integer = 0

        If cboProducts.Text.ToString Like "*PIZZA*" Then

            If cboExtraIngredient.Text = "" Then
                cont += 1
            Else
                dgvCart.Rows.Add(New String() {"", "", cboExtraIngredient.Text, cboExtraIngredient.SelectedValue, ""})
                total += Int(cboExtraIngredient.SelectedValue)
            End If

            If cboSpecialIngredient.Text = "" Then
                cont += 1
            Else
                dgvCart.Rows.Add(New String() {"", "", cboSpecialIngredient.Text, cboSpecialIngredient.SelectedValue, ""})
                total += Int(cboSpecialIngredient.SelectedValue)
            End If

        End If

        If cboProducts.Text.ToString Like "*RAVIOLES*" Or cboProducts.Text.ToString Like "*FETTUCCINI*" Or cboProducts.Text.ToString Like "*ÑOQUIS*" Or cboProducts.Text.ToString Like "*CANELONI*" Or cboProducts.Text.ToString Like "*LASAÑA*" Then
            If cboExtraIngredient.Text = "" Then
                cont += 1
            Else
                dgvCart.Rows.Add(New String() {"", "", cboExtraIngredient.Text, cboExtraIngredient.SelectedValue, ""})
                total += Int(cboExtraIngredient.SelectedValue)
            End If

            If cboSpecialIngredient.Text = "" Then
                cont += 1
            Else
                dgvCart.Rows.Add(New String() {"", "", cboSpecialIngredient.Text, cboSpecialIngredient.SelectedValue, ""})
                total += Int(cboSpecialIngredient.SelectedValue)
            End If

            If cboSauce.Text = "" Then
                cont += 1
            Else
                dgvCart.Rows.Add(New String() {"", "", cboSauce.Text, cboSauce.SelectedValue, ""})
                total += Int(cboSauce.SelectedValue)
            End If
        End If

        txtTotal.Text = total

        If Not txtPaymentValue.Text = "" Then
            txtChange.Text = Int(txtPaymentValue.Text) - Int(txtTotal.Text)
        End If

        clearBotLeftForm()



    End Sub
    Private Sub ventas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Size = Screen.PrimaryScreen.WorkingArea.Size

        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
        loadCboProducts()
        loadCboSauce()
        loadCboExtraIngredients()
        loadCboSpecialIngredients()
        Dim buttonColumn As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        buttonColumn.HeaderText = ""
        buttonColumn.Width = 60
        buttonColumn.Name = "buttonColumn"
        buttonColumn.Text = "Delete"
        buttonColumn.UseColumnTextForButtonValue = True
        dgvCart.Columns.Insert(5, buttonColumn)

    End Sub
    Public Sub loadCboProducts()

        Dim adap As SqlDataAdapter
        Dim dt As DataTable
        dt = New DataTable
        adap = New SqlDataAdapter("select price, name from product where type = 'PASTAS' OR  type ='PIZZA'  OR type ='PICOTEO' OR type ='OFERTA' order by name", con)
        adap.Fill(dt)

        cboProducts.DataSource = dt
        cboProducts.DisplayMember = "name"
        cboProducts.ValueMember = "price"
        cboProducts.SelectedIndex = -1

    End Sub
    Public Sub loadCboExtraIngredients()

        Dim adap As SqlDataAdapter
        Dim dt As DataTable
        dt = New DataTable
        adap = New SqlDataAdapter("select price, name from product where type = 'INGREDIENTES EXTRAS'", con)
        adap.Fill(dt)

        cboExtraIngredient.DataSource = dt
        cboExtraIngredient.DisplayMember = "name"
        cboExtraIngredient.ValueMember = "price"
        cboExtraIngredient.SelectedIndex = -1

    End Sub

    Public Sub loadCboSpecialIngredients()

        Dim adap As SqlDataAdapter
        Dim dt As DataTable
        dt = New DataTable
        adap = New SqlDataAdapter("select price, name from product where type = 'INGREDIENTES ESPECIALES'", con)
        adap.Fill(dt)

        cboSpecialIngredient.DataSource = dt
        cboSpecialIngredient.DisplayMember = "name"
        cboSpecialIngredient.ValueMember = "price"
        cboSpecialIngredient.SelectedIndex = -1

    End Sub

    Public Sub loadCboSauce()

        Dim adap As SqlDataAdapter
        Dim dt As DataTable
        dt = New DataTable
        adap = New SqlDataAdapter("select price, name from product where type = 'SALSAS'", con)
        adap.Fill(dt)

        cboSauce.DataSource = dt
        cboSauce.DisplayMember = "name"
        cboSauce.ValueMember = "price"
        cboSauce.SelectedIndex = -1

    End Sub
    Public Sub clearTopLeftForm()
        txtDescription.Text = ""
        cboProducts.SelectedIndex = -1
        txtQuantity.Text = 0
    End Sub

    Public Sub clearBotLeftForm()

        cboExtraIngredient.SelectedIndex = -1
        cboSpecialIngredient.SelectedIndex = -1
        cboSauce.SelectedIndex = -1

    End Sub

    Public Sub clearRightForm()
        cboDeliveryType.SelectedIndex = -1
        cboPaymentType.SelectedIndex = -1
        txtTotal.Text = 0
        txtClientName.Text = ""
        total = 0
        dgvCart.Rows.Clear()

    End Sub

    Public Sub deliveryCost()

        Dim adap As SqlDataAdapter
        Dim dt As DataTable
        dt = New DataTable
        adap = New SqlDataAdapter("select price, name from product where type = 'PASTAS' OR  type ='PIZZA'  OR type ='PICOTEO' OR type ='OFERTA' order by name", con)
        adap.Fill(dt)

        cboProducts.DataSource = dt
        cboProducts.DisplayMember = "name"
        cboProducts.ValueMember = "price"

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        clearBotLeftForm()
        clearTopLeftForm()

    End Sub

    Private Sub btnRemoveAll_Click(sender As Object, e As EventArgs) Handles btnRemoveAll.Click
        clearTopLeftForm()
        clearBotLeftForm()
        clearRightForm()
    End Sub

    Private Sub cboProducts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProducts.SelectedIndexChanged
        'Valdiar Inputs de agregados
        If cboProducts.Items.Count > 1 And cboProducts.Text <> "" Then

            If cboProducts.Text.ToString Like "*RAVIOLES*" Or cboProducts.Text.ToString Like "*FETTUCCINI*" Or cboProducts.Text.ToString Like "*ÑOQUIS*" Or cboProducts.Text.ToString Like "*CANELONI*" Or cboProducts.Text.ToString Like "*LASAÑA*" Then
                cboSauce.Enabled = True
                cboExtraIngredient.Enabled = True
                cboSpecialIngredient.Enabled = True
                txtQuantity.Enabled = False
                pastaCboValidation()
            ElseIf cboProducts.Text.ToString Like "*PIZZA*" Then
                cboSauce.Enabled = False
                cboExtraIngredient.Enabled = True
                cboSpecialIngredient.Enabled = True
                txtQuantity.Enabled = False


            ElseIf cboProducts.Text.ToString Like "*OFERTA*" Then
                cboSauce.Enabled = False
                cboExtraIngredient.Enabled = False
                cboSpecialIngredient.Enabled = False
                txtQuantity.Enabled = False

            Else
                cboSauce.Enabled = False
                cboExtraIngredient.Enabled = False
                cboSpecialIngredient.Enabled = False
                txtQuantity.Enabled = True
            End If

        End If

    End Sub

    Private Sub btnStoreSale_Click(sender As Object, e As EventArgs) Handles btnStoreSale.Click
        If MessageBox.Show("SEGURO QUE DESEA REALIZAR LA VENTA?", "VENTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then

            Dim cont As Integer
            Dim address As String = "local"
            cont = 0
            If cboDeliveryType.SelectedIndex < 0 Then
                cont += 1
            End If
            If cboPaymentType.SelectedIndex < 0 Then
                cont += 1
            End If
            If txtClientName.Text = "" Then
                cont += 1
            End If

            If cboDeliveryType.Text = "DELIVERY" Then
                If txtAddress.Text = "" Then
                    cont += 1

                Else
                    address = txtAddress.Text
                End If

            End If

            If dgvCart.Rows.Count < 1 Then
                cont += 1
            End If

            If cont = 0 Then

                cmd = con.CreateCommand()
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "insert into sale_order (date, paymenttype, clientName, login_id, status, delivery, address) values('" + Format(Now(), "yyyy/MM/dd HH:mm:ss") + "', '" + cboPaymentType.Text + "', '" + txtClientName.Text + "', '1', 1, '" + cboDeliveryType.Text + "', '" + address + "')"
                cmd.ExecuteNonQuery()

                'Traigo el dato de la ultima venta para asignarlo al detalle
                Dim sql As String = "SELECT TOP 1 * FROM sale_order ORDER BY ID DESC"
                Dim adapter As SqlDataAdapter = New SqlDataAdapter(sql, con)
                Dim dtDatos As DataTable = New DataTable
                adapter.Fill(dtDatos)
                Dim saleOrderId As String = dtDatos.Rows(0)("id")

                Dim product As String
                Dim added As String
                Dim quantity As Integer

                'Creo Sale Order Details por cada fila en el dgv

                For index As Integer = 0 To dgvCart.Rows.Count - 1

                    If dgvCart.Rows(index).Cells(2).Value <> "" Then
                        added = dgvCart.Rows(index).Cells(2).Value

                    Else
                        added = ""
                    End If

                    If dgvCart.Rows(index).Cells(0).Value <> "" Then
                        product = dgvCart.Rows(index).Cells(0).Value

                    Else
                        product = ""
                    End If

                    cmd = con.CreateCommand()
                    cmd.CommandType = CommandType.Text
                    If product <> "" Then
                        cmd.CommandText = "insert into sale_order_details (quantity, total, sale_order_id, product_id, added, productName, description) values(" + dgvCart.Rows(index).Cells(1).Value + ", " + dgvCart.Rows(index).Cells(3).Value + ", " + saleOrderId + ", 172, '" + added + "', '" + product + "', '" + dgvCart.Rows(index).Cells(4).Value + "')"
                        cmd.ExecuteNonQuery()
                        descontarEnvase(product, dgvCart.Rows(index).Cells(1).Value)
                    Else
                        cmd.CommandText = "insert into sale_order_details (quantity, total, sale_order_id, product_id, added, productName, description) values(1, " + dgvCart.Rows(index).Cells(3).Value + ", " + saleOrderId + ", 172, '" + added + "', '" + product + "', '" + dgvCart.Rows(index).Cells(4).Value + "')"
                        cmd.ExecuteNonQuery()
                    End If

                Next

                MessageBox.Show("VENTA CREADA")
                imprimir()
                clearBotLeftForm()
                clearTopLeftForm()
                clearRightForm()


            Else
                MessageBox.Show("COMPLETAR TODOS LOS CAMPOS")
            End If

        End If


    End Sub

    Private Sub cboDeliveryType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDeliveryType.SelectedIndexChanged
        If cboDeliveryType.Text = "DELIVERY" Then
            txtAddress.Visible = True
            txtAddress.Text = ""
            lblAddress.Visible = True

            If delivered = False And txtTotal.Text <> "" Then

                total = total + 1000
                txtTotal.Text = total
                delivered = True
            End If

        Else
            txtAddress.Visible = False
            txtAddress.Text = ""
            lblAddress.Visible = False

            If delivered And txtTotal.Text <> "" Then

                total = total - 1000
                txtTotal.Text = total
                delivered = False
            End If

        End If

    End Sub

    Private Sub txtPaymentValue_TextChanged(sender As Object, e As EventArgs) Handles txtPaymentValue.TextChanged
        If Integer.TryParse(txtPaymentValue.Text, Nothing) And Integer.TryParse(txtTotal.Text, Nothing) Then

            txtChange.Text = Convert.ToInt32(txtPaymentValue.Text) - Convert.ToInt32(txtTotal.Text)

        End If

    End Sub

    Private Sub btnAddProduct_Click(sender As Object, e As EventArgs) Handles btnAddProduct.Click

        Dim cont As Integer = 0
        Dim quantity As Integer

        If cboProducts.Text = "" Then
            cont += 1
        End If

        If cboProducts.Text.ToString Like "*RAVIOLES*" Or cboProducts.Text.ToString Like "*FETTUCCINI*" Or cboProducts.Text.ToString Like "*ÑOQUIS*" Or cboProducts.Text.ToString Like "*CANELONI*" Or cboProducts.Text.ToString Like "*LASAÑA*" Or cboProducts.Text.ToString Like "*PIZZA*" Or cboProducts.Text.ToString Like "*OFERTA*" Then
            quantity = 1
        Else
            If txtQuantity.Text = "" Then
                cont += 1
            Else
                If Convert.ToInt32(txtQuantity.Text) > 0 Then
                    quantity = Convert.ToInt32(txtQuantity.Text)
                Else
                    cont += 1
                End If

            End If

        End If

        If (cboProducts.Text.ToString Like "*BEBIDAS*" Or cboProducts.Text.ToString Like "*OFERTA*") And txtDescription.Text = "" Then
            cont += 1
        End If


        If cont = 0 Then
            dgvCart.Rows.Add(New String() {cboProducts.Text, quantity, "", Convert.ToInt32(cboProducts.SelectedValue) * quantity, txtDescription.Text})

            total += (Int(cboProducts.SelectedValue) * quantity)

            txtTotal.Text = total
            txtDescription.Text = ""

            If Not txtPaymentValue.Text = "" Then
                txtChange.Text = Int(txtPaymentValue.Text) - Int(txtTotal.Text)
            End If
        Else
            MessageBox.Show("COMPLETES LOS CAMPOS PARA AGREGAR UN PRODUCTO")
        End If

    End Sub

    Private Sub cboPaymentType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPaymentType.SelectedIndexChanged
        If cboPaymentType.Text = "EFECTIVO" Then
            txtPaymentValue.Visible = True
            txtChange.Visible = True
            lblPaymentType.Visible = True
            lblChange.Visible = True

        Else
            txtPaymentValue.Visible = False
            txtChange.Visible = False
            lblPaymentType.Visible = False
            lblChange.Visible = False
        End If
    End Sub


    Public Function descontarEnvase(product, quantity)

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text

        If product.ToString Like "*RAVIOLES*" Or product.ToString Like "*FETTUCCINI*" Or product.ToString Like "*ÑOQUIS*" Or product.ToString Like "*CANELONI*" Or product.ToString Like "*LASAÑA*" Then
            cmd.CommandText = "Update envase set cantidad = cantidad - " + quantity + " where nombre = 'Caja'"
            cmd.ExecuteNonQuery()
        ElseIf product.ToString Like "*PIZZA*" Then

            cmd.CommandText = "Update envase set cantidad = cantidad - " + quantity + " where nombre = 'Envase'"
            cmd.ExecuteNonQuery()

        End If




    End Function

    Public Function datos_varios()

        Dim con3 As New SqlConnection

        con3.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con3.State = ConnectionState.Open Then
            con3.Close()
        End If
        con3.Open()

        Dim cmd As New SqlCommand("Select TOP 1 so.id, so.Date, so.address, so.clientName, sum(sod.total) as total , so.delivery from sale_order so 
 Join sale_order_details sod
On so.id = sod.sale_order_id
 Group by so.id, so.date, so.address,so.clientName, so.delivery ORDER BY so.id DESC", con3)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader
        Dim myList As New List(Of String)()
        While dr.Read
            myList.Add(dr("date").ToString)
            myList.Add(dr("clientName").ToString)
            myList.Add(dr("address").ToString)
            myList.Add(dr("total").ToString)
            myList.Add(dr("delivery").ToString)
        End While
        Return myList
        dr.Close()

    End Function


    Public Function imprimir()

        'ruta
        Dim path As String
        path = "C:\\Users\\Public\\Documents\\COMANDA.docx"

        'Load Document
        Dim doc As New Document()
        doc.LoadFromFile(path)

        Dim myList As New List(Of String)()
        myList = datos_varios()

        If myList.Count > 0 Then

            Dim array As String() = myList.ToArray
            doc.Replace("[HORA]", array(0).ToString, False, True)
            doc.Replace("[CLIENTNAME]", array(1).ToString, False, True)
            doc.Replace("[ADDRESS]", array(2).ToString, False, True)
            If LCase(array(4).ToString()) = "local" Then
                doc.Replace("[TOTAL]", array(3).ToString, False, True)
            Else
                doc.Replace("[TOTAL]", Convert.ToInt32(array(3).ToString) + 1000, False, True)


            End If

            Dim selection As TextSelection = doc.FindString("[tabla]", True, True)
            Dim Range As TextRange = selection.GetAsOneRange()
            Dim Paragraph As Paragraph = Range.OwnerParagraph
            Dim Body As Body = Paragraph.OwnerTextBody
            Dim Index As Integer = Body.ChildObjects.IndexOf(Paragraph)
            Body.ChildObjects.Remove(Paragraph)
            Dim table As Table = doc.AddSection().AddTable(True)


            Dim Header() As String = {"NOMBRE DEL PRODUCTO", "AGREGADOS", "CANTIDAD", "DESCRIPCION"}
            Dim data As String(,) = datos_tabla()

            Dim dataLength = (Convert.ToInt32(data.Length) / 4) - 1

            table.ResetCells((Convert.ToInt32(data.Length) / 4) + 1, Header.Length)
            Body.ChildObjects.Insert(Index, table)

            'Header Row

            Dim FRow As TableRow = table.Rows(0)

            FRow.IsHeader = True

            'Row Height

            FRow.Height = 6
            'Header Format

            FRow.RowFormat.BackColor = Color.AliceBlue

            For i As Integer = 0 To Header.Length - 1

                'Cell Alignment

                Dim p As Paragraph = FRow.Cells(i).AddParagraph()

                FRow.Cells(i).CellFormat.VerticalAlignment = VerticalAlignment.Middle

                p.Format.HorizontalAlignment = HorizontalAlignment.Center

                'Data Format

                Dim TR As TextRange = p.AppendText(Header(i))

                TR.CharacterFormat.FontName = "arial"

                TR.CharacterFormat.FontSize = 4

                TR.CharacterFormat.TextColor = Color.Black

                TR.CharacterFormat.Bold = True

            Next i

            'Data Row

            For r As Integer = 0 To dataLength

                Dim DataRow As TableRow = table.Rows(r + 1)

                'Row Height

                DataRow.Height = 8

                'C Represents Column.

                For c As Integer = 0 To 3

                    'Cell Alignment

                    DataRow.Cells(c).CellFormat.VerticalAlignment = VerticalAlignment.Middle

                    'Fill Data in Rows

                    Dim p2 As Paragraph = DataRow.Cells(c).AddParagraph()

                    Dim TR2 As TextRange = p2.AppendText(data(r, c))

                    'Format Cells

                    p2.Format.HorizontalAlignment = HorizontalAlignment.Left
                    TR2.CharacterFormat.FontName = "Calibri"
                    TR2.CharacterFormat.FontSize = 8
                    TR2.CharacterFormat.TextColor = Color.Black

                Next c

            Next r

            doc.Sections(0).Tables(0).Rows(0).Cells(0).Width = 20

            'Save And Launch
            'doc.Close()
            'doc.SaveToFile("C:\\Users\\Public\\Documents\\COMANDA.PDF", Spire.Doc.FileFormat.PDF)
            'System.Diagnostics.Process.Start("COMANDA.PDF")


            doc.SaveToFile("C:\\Users\\Public\\Documents\\COMANDA.PDF", Spire.Doc.FileFormat.PDF)
            doc.Close()
            Dim PrintPDF As New ProcessStartInfo
            PrintPDF.UseShellExecute = True
            PrintPDF.Verb = "print"
            PrintPDF.WindowStyle = ProcessWindowStyle.Hidden
            PrintPDF.FileName = "C:\\Users\\Public\\Documents\\COMANDA.PDF"
            Process.Start(PrintPDF)


        End If




    End Function

    Public Function datos_tabla()
        Dim con2 As New SqlConnection

        con2.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con2.State = ConnectionState.Open Then
            con2.Close()
        End If
        con2.Open()

        Dim contador As Integer = 0
        Dim cmd As New SqlCommand("select sod.quantity, sod.productName, sod.added, so.date, sod.description from sale_order
so join sale_order_details sod on so.id = sod.sale_order_id WHERE so.id = (select TOP 1 id from sale_order ORDER BY id DESC)", con2)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader
        While dr.Read
            contador += 1
        End While
        dr.Close()


        Dim data As String(,) = New String(contador - 1, 3) {}
        contador = 0
        dr = cmd.ExecuteReader
        While dr.Read

            data(contador, 0) = dr("productName").ToString
            data(contador, 1) = dr("added").ToString
            data(contador, 2) = dr("quantity").ToString
            data(contador, 3) = dr("description").ToString
            contador = contador + 1
        End While
        Return data
        dr.Close()

    End Function

    Private Sub cboSauce_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSauce.SelectedIndexChanged

        pastaCboValidation()

    End Sub

    Public Sub pastaCboValidation()

        If cboProducts.Text.ToString Like "*RAVIOLES*" Or cboProducts.Text.ToString Like "*FETTUCCINI*" Or cboProducts.Text.ToString Like "*ÑOQUIS*" Or cboProducts.Text.ToString Like "*CANELONI*" Or cboProducts.Text.ToString Like "*LASAÑA*" Or cboProducts.Text.ToString Like "*PIZZA*" Then

            If cboSauce.Text <> "" Then
                cboExtraIngredient.Enabled = True
                cboSpecialIngredient.Enabled = True
            Else
                cboExtraIngredient.Enabled = False
                cboSpecialIngredient.Enabled = False
            End If
        End If
    End Sub

    Private Sub txtQuantity_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtQuantity.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
        If Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            MsgBox("SOLO PUEDE DIGITAR NÚMEROS")
        End If
    End Sub

    Private Sub txtPaymentValue_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPaymentValue.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
        If Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            MsgBox("SOLO PUEDE DIGITAR NÚMEROS")
        End If
    End Sub

    Private Sub dgvCart_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCart.CellContentClick
        If e.ColumnIndex = 5 Then
            Dim row As DataGridViewRow = dgvCart.Rows(e.RowIndex)
            total -= row.Cells(3).Value
            txtTotal.Text = total
            dgvCart.Rows.RemoveAt(Convert.ToInt32(row.Index))
        End If
    End Sub
End Class